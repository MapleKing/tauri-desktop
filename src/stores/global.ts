import { defineStore } from "pinia";
import { updateUserConfig } from "@/frame/frame";
const useGlobal = defineStore("global",{
    state:()=>{
        return {
            userConfig:{} as any,
            menus:[] as any[],
            theme:"dark"
        }
    },
    actions:{
        updateTheme(){
            this.userConfig.theme = this.userConfig.theme === "dark" ? "light" : "dark"
            this.theme = this.userConfig.theme
            updateUserConfig()
        }
    }
})

export {useGlobal} 