  /**
   * 获取本地图
   * @param name // 文件名 如 doc.png
   * @returns {*|string}
   */
  function getAssetsImage(name: string) {
    return new URL(`/src/assets/image/${name}`, import.meta.url).href;
  }


  export {
    getAssetsImage
  }