import { createApp } from "vue";
import App from "./App.vue";
import SvgIcon from "@/components/svg-icon.vue";
import "@/assets/iconfont.js";
import { createPinia } from "pinia";
import router from "@/router/index.ts";

const app = createApp(App);
app.use(createPinia())
app.component("SvgIcon", SvgIcon)
app.use(router)
app.mount("#app");
