import { createRouter, createWebHistory } from "vue-router";
import MainIndex from "@/views/layout/index.vue";
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "Home",
      redirect: "/index",
    },
    {
      path: "/index",
      name: "index",
      component: MainIndex,
    }
  ],
});

export default router;
