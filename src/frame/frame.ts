import { resolveResource } from '@tauri-apps/api/path';
import { readTextFile,writeTextFile} from '@tauri-apps/plugin-fs';
import { useGlobal } from '@/stores/global';
import CONFIG from '@/frame/config';
const initMenus = async function (){
  const resourcePath = await resolveResource(CONFIG.CONFIG_PATH);
  useGlobal().menus = JSON.parse(await readTextFile(resourcePath)); 
}
const initUserConfig = async function (){
    const resourcePath = await resolveResource(CONFIG.USER_CONFIG_PATH);
    //获取用户配置
    //获取用户本地配置的主题
    useGlobal().userConfig = JSON.parse(await readTextFile(resourcePath)); 
    useGlobal().theme = useGlobal().userConfig.theme;
}


const initFrame = async function (){
    return new Promise(async (resolve) => {
        await initMenus();
        await initUserConfig();
        resolve(true);
    })
}
const updateUserConfig = async function (){
    const userConfig = useGlobal().userConfig; // 获取用户配置
    const resourcePath = await resolveResource(CONFIG.USER_CONFIG_PATH);
    await writeTextFile(resourcePath, JSON.stringify(userConfig)); // 将用户配置写入文件
}

export { 
    initFrame,
    updateUserConfig
};