const CONFIG = {
    CONFIG_PATH: 'config/config.json',
    USER_CONFIG_PATH: 'config/user_config.json',
}
export default CONFIG;